/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.arquivos.lib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;


/**
 *
 * @author developer
 */
public class Salvador {

    public static Object carrega(String nomeArq) {

        Object p = null;
        
        try {
            FileInputStream fos = new FileInputStream(nomeArq);
            ObjectInputStream ois = new ObjectInputStream(fos);
            p = ois.readObject();
            System.out.println(p);
            
            fos.close();
            ois.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
        return p;
    }
    
    public static void salva(Object praSalvar, String nomeArq){
        try{
            FileOutputStream fos = new FileOutputStream(nomeArq, true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(praSalvar);
            oos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

