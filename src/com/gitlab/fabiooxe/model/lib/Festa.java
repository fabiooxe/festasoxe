/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.model.lib;

import java.io.Serializable;
import java.util.Date;
import javafx.scene.control.DatePicker;

/**
 *
 * @author Fábio
 */
public class Festa implements Serializable{
    private String nome;
    private String data;
    private String tipo;
    private String descricao;
    private String local;
    private int num;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    
    public Ingresso gerarIngresso(Cliente c){
        num--;
        return new Ingresso(c, this);
    }

    @Override
    public String toString() {
        return "Festa{" + "nome=" + nome + ", data=" + data + ", tipo=" + tipo + ", descricao=" + descricao + ", local=" + local + ", num=" + num + '}';
    }

    public class Integer {

        public Integer() {
        }
    }
    
}
