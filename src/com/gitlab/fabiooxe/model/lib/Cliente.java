/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.model.lib;

import java.io.Serializable;
import java.util.ArrayList;

/**nome, sexo e-mail
 *
 * @author Fábio
 */
public class Cliente implements Serializable{
    private String nome;
    private String sexo;
    private String email;
    private String senha;
    private int pontos = 0;
    private static int codigo = 1;
    ArrayList <Ingresso> ing = new ArrayList <> ();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public ArrayList<Ingresso> getIng() {
        return ing;
    }

    public void setIng(ArrayList<Ingresso> ing) {
        this.ing = ing;
    }
    
    
    public void comprar(Ingresso i){
        ing.add(i);
        pontos++;
        codigo++;
    }
@Override
    public String toString() {
        return '+'+ email;
    }   
}


  