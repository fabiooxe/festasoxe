/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.view.lib;

import com.gitlab.fabiooxe.festa.lib.TrocadoraDeTelaUtil;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Fábio
 */
public class AreaClienteController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Label pontosL;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
        @FXML
        public void cliqueiEmVoltar(){
        TrocadoraDeTelaUtil.trocaTela("FXMLDocument");
    }
    
        @FXML
        public void pontuacao(){
            Label label = new Label();  
            label.setText("Seus pontso: " + pontosL);

        }
}
