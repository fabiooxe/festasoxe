/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.view.lib;

import com.gitlab.fabiooxe.festa.lib.TrocadoraDeTelaUtil;
import com.gitlab.fabiooxe.model.lib.Cliente;
import com.gitlab.fabiooxe.model.lib.Festa;
import com.gitlab.fabiooxe.model.lib.Ingresso;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author Fábio
 */
public class FXMLDocumentController implements Initializable {
    
    private Festa f = new Festa();  
    private Cliente c = new Cliente();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void cliqueiEmComprar(){
        Ingresso i = f.gerarIngresso(c);
        c.comprar(i);
        System.out.println("ooo" + i);
    }
   
    @FXML
    public void cliqueiEmFesta(){
        TrocadoraDeTelaUtil.trocaTela("CriarFestaController");

    }
    
    @FXML
        public void cliqueiEmComprarIng(){
        TrocadoraDeTelaUtil.trocaTela("ComprarIngresso");
    }

            @FXML
        public void cliqueiEmVerPontos(){
        TrocadoraDeTelaUtil.trocaTela("AreaCliente");
    }
}
