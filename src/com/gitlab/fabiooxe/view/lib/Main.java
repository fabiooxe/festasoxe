/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.view.lib;

import com.gitlab.fabiooxe.arquivos.lib.Salvador;
import static com.gitlab.fabiooxe.arquivos.lib.Salvador.carrega;
import static com.gitlab.fabiooxe.arquivos.lib.Salvador.salva;
import com.gitlab.fabiooxe.model.lib.Cliente;
import com.gitlab.fabiooxe.model.lib.Festa;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Fábio
 */
public class Main extends Application {
    
    public static Stage stage; 
    public static ArrayList <Festa> f = (ArrayList <Festa>) Salvador.carrega("festas.oxi");
    public static ArrayList <Cliente> c = (ArrayList <Cliente>) Salvador.carrega("clientes.oxi");
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            this.stage =stage;
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
            f = (ArrayList <Festa>) Salvador.carrega("festas.oxi");
            c = (ArrayList <Cliente>) Salvador.carrega("clientes.oxi");
        
            if (f == null) {
                System.out.println("Festas não encontradas!");
                f = new ArrayList<>();
            }
            
            if (c == null) {
                System.out.println("Festas não encontradas!");
                c = new ArrayList<>();
            }
        
        launch(args);    
    }  
     
    public void verPontos(Cliente c){
       System.out.println(c.getPontos());
    }
    
    private int x = 0;
     Festa p = new Festa();
    ArrayList <Festa> fes = new ArrayList <> ();
    
    public void armazenarFesta(Festa p){
        fes.add(p);
        carrega("MeuArg");
        salva(p,"Arq");
        x++;
    }
    
}

