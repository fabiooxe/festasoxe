/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.view.lib;

import com.gitlab.fabiooxe.arquivos.lib.Salvador;
import com.gitlab.fabiooxe.festa.lib.TrocadoraDeTelaUtil;
import com.gitlab.fabiooxe.model.lib.Festa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Fábio
 */
public class CriarFestaController implements Initializable {

    @FXML
    private TextField nomeTF;
    
     @FXML
    private TextField tipoTF;
     
      @FXML
    private TextField localTF;
      
       @FXML
    private TextField dataTF;
       
       @FXML
    private TextField descricaoTF;
       
        @FXML
    private TextField ingTF;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
    @FXML
    public void adicionaFesta(){
    
        Festa fes = new Festa();
        fes.setNome(nomeTF.getText());
        fes.setTipo(tipoTF.getText());
        fes.setLocal(localTF.getText());
        fes.setDescricao(descricaoTF.getText());
        fes.setData(dataTF.getText());
        fes.setNum(Integer.parseInt(ingTF.getText()));
        System.out.println("Festa" + fes);
        Main.f.add(fes);
        Salvador.salva(Main.f, "festas.oxi");
        System.out.println("Festa" + fes);
    }
    
     @FXML
        public void cliqueiEmVoltar(){
        TrocadoraDeTelaUtil.trocaTela("FXMLDocument");
    }
    
    
    
}
