/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.view.lib;

import com.gitlab.fabiooxe.arquivos.lib.Salvador;
import com.gitlab.fabiooxe.festa.lib.TrocadoraDeTelaUtil;
import com.gitlab.fabiooxe.model.lib.Cliente;
import com.gitlab.fabiooxe.model.lib.Festa;
import com.gitlab.fabiooxe.model.lib.Ingresso;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Fábio
 */
public class ComprarIngressoController implements Initializable {

    /**
     * Initializes the controller class.
     */
     
     @FXML
    private TextField emailTF;
    
     @FXML
    private PasswordField senhaTF;
     
      @FXML
    private TextField sexoTF;
     
      @FXML
      private ListView<Festa> festas;
      private ObservableList<Festa> festasObs;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        festasObs = festas.getItems();
        
        festasObs.addAll(Main.f);
    } 
    
    @FXML
    public void logar (){
        Cliente cli = new Cliente();
        cli.setEmail(emailTF.getText());
        cli.setSenha(senhaTF.getText());
        cli.setSexo(sexoTF.getText());
        //cli.setIng(Integer.parseInt(cli.getIng().getText()));
        Main.c.add(cli);
        Salvador.salva(Main.c, "clientes.oxi");
        System.out.println("Cliente" + cli);
    }
   
         @FXML
        public void cliqueiEmVoltar(){
        TrocadoraDeTelaUtil.trocaTela("FXMLDocument");
    }
        
        @FXML
        public void comprar(){
            
            Festa f = festas.getSelectionModel().getSelectedItem();
        
            String email = emailTF.getText();
            String senha = senhaTF.getText();
            String sexo = sexoTF.getText();
            
            Cliente c = null;
            
            // for do capeta pra pegar o usuário
            for(Cliente x : Main.c) {
                if (x.getEmail().equals(email) && x.getSenha().equals(senha) && x.getSexo().equals(sexo)) {
                    c = x;
                }
            }
            
            // Se não achou ninguem assim, cria um novo
            if (c == null) {
                c = new Cliente();
                c.setEmail(email);
                c.setSenha(senha);
                c.setSexo(sexo);
                
                Main.c.add(c);
            }
            
            if (f != null && c != null) {
                Ingresso i = f.gerarIngresso(c);
                c.comprar(i);
            }
            
            Salvador.salva(Main.c, "clientes.oxi");
        }
}

