/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fabiooxe.festa.lib;

import com.gitlab.fabiooxe.view.lib.Main;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Fábio
 */
public class TrocadoraDeTelaUtil {

    public static void trocaTela(String fileFxml) {
        Stage stage = Main.stage;
        FXMLLoader loader = new FXMLLoader(Main.class.getResource( fileFxml + ".fxml"));
        Parent root;
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
             stage.setTitle("CriarFestaController");
        stage.show();
        
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Senhor programador Verifique o nome de " + fileFxml + " Exciste");
        }
        
    }

}
